import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.JsonAdapter;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.queries.groups.GroupsGetMembersQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

public class ActionHelper {


    public static void sendById(Integer id, String body){
         Random random = new Random();
         Logger LOG = LoggerFactory.getLogger(CallbackApiLongPollHandler.class);
        try {
            application.vkApiClient.messages().send(application.groupActor).message(body).userId(id).randomId(random.nextInt()).execute();
        } catch (ApiException e) {
            LOG.error("INVALID REQUEST", e);
        } catch (ClientException e) {
            LOG.error("NETWORK ERROR", e);
        }
    }

    public static void sendToFollowers(String body){
        Logger LOG = LoggerFactory.getLogger(CallbackApiLongPollHandler.class);
        String url = "https://api.vk.com/method/groups.getMembers?group_id=183276628&access_token=e37233d64bec7791a554144c5dfda758ac454da57608afead46f4e469d45c03f0088e9196c18a6cb0be9a&v=5.0";
        try {
            URL obj = new URL(url);
            HttpURLConnection connection = null;
            connection = (HttpURLConnection) obj.openConnection();
            connection.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            Gson g = new Gson();
            MemberList list = g.fromJson(String.valueOf(response), MemberList.class);
            System.out.println(list.toString());

            for(int i=0;i<list.getResponse().getUsers().length;i++){
                ActionHelper.sendById(list.getResponse().getUsers()[i],body);
            }



        } catch (IOException e) {
            e.printStackTrace();
        }

    }



}

