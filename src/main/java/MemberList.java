import java.util.Arrays;

public class MemberList {
    private Data response;

    public Data getResponse() {
        return response;
    }

    public void setResponse(Data response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return response.toString();
    }

    public MemberList(){

    }
}

class Data{
    private Integer count;
    private Integer[] users;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer[] getUsers() {
        return users;
    }

    public void setUsers(Integer[] users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "Data{" +
                "count=" + count +
                ", users=" + Arrays.toString(users) +
                '}';
    }
}

