import com.vk.api.sdk.callback.longpoll.CallbackApiLongPoll;
import com.vk.api.sdk.callback.objects.group.CallbackGroupJoin;
import com.vk.api.sdk.callback.objects.group.CallbackGroupLeave;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.GroupActor;

import com.vk.api.sdk.client.actors.UserActor;

import com.vk.api.sdk.objects.messages.Message;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class CallbackApiLongPollHandler extends CallbackApiLongPoll {

    private static final Logger LOG = LoggerFactory.getLogger(CallbackApiLongPollHandler.class);

    public CallbackApiLongPollHandler(VkApiClient client, UserActor actor, Integer groupId) {
        super(client, actor, groupId);
    }

    public CallbackApiLongPollHandler(VkApiClient client, GroupActor actor) {
        super(client, actor);
    }

    public void messageNew(Integer groupId, Message message) {
        System.out.println("MESSAGE: " + message.getUserId());
        LOG.info("messageNew: " + message.getBody() + " ID: " + message.getFromId());
        Analyser.analyse(message.getBody(),message.getUserId());

    }

    public void messageReply(Integer groupId, Message message) {
        LOG.info("messageReply: " + message.toString());
    }

    public void messageEdit(Integer groupId, Message message) {
        LOG.info("messageReply: " + message.toString());
    }


    public void groupJoin(Integer groupId, CallbackGroupJoin message) {
        ActionHelper.sendById(message.getUserId(), "О, дарова");

    }
    public void groupLeave(Integer groupId, CallbackGroupLeave message) {
        ActionHelper.sendById(message.getUserId(), "Может быть ты не будешь уходить?(");
    }


}