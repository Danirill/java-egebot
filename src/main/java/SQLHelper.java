import java.io.UnsupportedEncodingException;
import java.sql.*;

/**
 * Simple Java program to connect to MySQL database running on localhost and
 * running SELECT and INSERT query to retrieve and add data.
 * @author Javin Paul
 */
public class SQLHelper {
    // JDBC URL, username and password of MySQL server
    private static final String url = "jdbc:mysql://95.214.63.51:3306/egebot";
    private static final String user = "test";
    private static final String password = "pass";

    // JDBC variables for opening and managing connection
    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs;

    public static int createExersize(String text, String answer, int type){

        try {
            text = new String( text.getBytes(), "UTF-8" );
            text.c
            System.out.print("AFTER: " + text);
            con = DriverManager.getConnection(url, user, password);
            stmt = con.createStatement();
            int flag = stmt.executeUpdate("INSERT INTO Exersizes(Text,Answer,Type) VALUES ('" +text + "','"+ answer +"','" + type +  "')");
            con.close();
            stmt.close();
            return flag;

        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return -1;
        }

    }

    public static int createClient(int id, int answers, int achievments){
        try {
            con = DriverManager.getConnection(url, user, password);
            stmt = con.createStatement();
            int flag = stmt.executeUpdate("INSERT INTO Students(VK_ID,Success_answers,Achievments) VALUES ('" +id + "','"+ answers +"','" + achievments +  "')");
            con.close();
            stmt.close();
            return flag;
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static ResultSet getExsersize(int ExersizeId){
        try {
            con = DriverManager.getConnection(url, user, password);
            stmt = con.createStatement();
            ResultSet resultSet = stmt.executeQuery("SELECT * FROM Exersizes WHERE ID="+ExersizeId);
            con.close();
            stmt.close();
            return resultSet;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isClient(int Student_VK_ID){
        try {
            con = DriverManager.getConnection(url, user, password);
            stmt = con.createStatement();
            ResultSet resultSet = stmt.executeQuery("SELECT * FROM Students WHERE VK_ID="+Student_VK_ID);
            con.close();
            stmt.close();
            return resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }

}